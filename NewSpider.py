# -!- coding: utf-8 -*-

__author__ = 'faith'

import urllib2, re, os, datetime
from selenium import webdriver
from selenium.webdriver.common.by import By


class Spider:
    def __init__(self):
        self.page = 1
        self.dirName = 'MMSpider'
        # 这是一些配置 关闭loadimages可以加快速度 但是第二页的图片就不能获取了打开(默认)
        cap = webdriver.DesiredCapabilities.PHANTOMJS
        cap["phantomjs.page.settings.resourceTimeout"] = 1000
        # cap["phantomjs.page.settings.loadImages"] = False
        # cap["phantomjs.page.settings.localToRemoteUrlAccessEnabled"] = True
        self.driver = webdriver.PhantomJS(desired_capabilities=cap)

    # 获取页面内容提取
    def LoadPageContent(self):
        # 记录开始时间
        begin_time = datetime.datetime.now()
        url = "http://www.kuaidaili.com/proxylist/1/"

        USER_AGENT = 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.130 Safari/537.36'
        headers = {'User-Agent': USER_AGENT}

        request = urllib2.Request(url, headers=headers)
        response = urllib2.urlopen(request)
        print response.status_code

if __name__ == '__main__':
    browser = webdriver.Chrome('/Users/faith/Downloads/chromedriver')
    browser.get('http://www.kuaidaili.com/proxylist/1/')
    # print(browser.page_source)
    elems = browser.find_elements(By.XPATH, './/div[@id="index_free_list"]//tbody/tr')
    for elem in elems:
        proxies = elem.find_elements(By.XPATH, './td')[0:2]
        ip_info = []
        for proxy in proxies:
            # print elem.get_attribute('innerHTML')
            ip_info.append(proxy.get_attribute('innerHTML'))
        print ':'.join(ip_info)
