# -!- coding: utf-8 -*-
from Util.utilFunction import singleton
from selenium import webdriver
from selenium.webdriver.common.by import By

__author__ = 'faith'


@singleton
class WebMonitor(object):
    """
    web 模拟器
    """

    def __init__(self):
        self.browser = webdriver.Chrome('/Users/faith/Downloads/chromedriver')

    def get_html(self, url):
        self.browser.get(url)
        elements = self.browser.find_elements(By.XPATH, './/div[@id="index_free_list"]//tbody/tr')
        for elem in elements:
            proxies = elem.find_elements(By.XPATH, './td')[0:2]
            ip_info = []
            for proxy in proxies:
                ip_info.append(proxy.get_attribute('innerHTML'))
            ips = ':'.join(ip_info)
            print ips
            yield ips
