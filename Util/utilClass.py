# -*- coding: utf-8 -*-
# !/usr/bin/env python
"""
-------------------------------------------------
   File Name：     utilClass.py  
   Description :  tool class
   Author :       JHao
   date：          2016/12/3
-------------------------------------------------
   Change Activity:
                   2016/12/3: Class LazyProperty
                   2016/12/4: rewrite ConfigParser
-------------------------------------------------
"""
import time
from json import JSONEncoder, loads
import requests
import logging

from decimal import Decimal
from requests.packages.urllib3.exceptions import InsecureRequestWarning, InsecurePlatformWarning
import pickle
import json

from Util.utilFunction import singleton

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
requests.packages.urllib3.disable_warnings(InsecurePlatformWarning)

__author__ = 'faith'


class LazyProperty(object):
    """
    LazyProperty
    explain: http://www.spiderpy.cn/blog/5/
    """

    def __init__(self, func):
        self.func = func

    def __get__(self, instance, owner):
        if instance is None:
            return self
        else:
            value = self.func(instance)
            setattr(instance, self.func.__name__, value)
            return value


from ConfigParser import ConfigParser


class ConfigParse(ConfigParser):
    """
    rewrite ConfigParser, for support upper option
    """

    def __init__(self):
        ConfigParser.__init__(self)

    def optionxform(self, optionstr):
        return optionstr


class Singleton(type):
    """
    Singleton Metaclass
    """

    _inst = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._inst:
            cls._inst[cls] = super(Singleton, cls).__call__(*args)
        return cls._inst[cls]


class HttpUtil(object):
    """
    http util
    """
    agent = ('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) '
             'Chrome/53.0.2785.143 Safari/537.36')
    headers = {
        # "Host": "www.zhihu.com",
        # "Referer": "https://www.zhihu.com/",
        'User-Agent': agent
    }

    def __init__(self):
        self.session = requests.Session()

    def get_html(self, url, cookies=None, encoding='gb2312'):
        # if self.cookies
        try:
            # self.headers['Host'] = 'www.kuaidaili.com'
            # self.headers['Referer'] = 'http://www.kuaidaili.com/proxylist/1/?yundun=9ece30dff3f0053b5101'
            for i in range(1, 4):
                html = self.session.get(url, headers=self.headers, timeout=10, verify=False)
                print html.status_code
                if html.status_code == requests.codes.ok:
                    html.encoding = encoding
                    return html.text
                else:
                    time.sleep(1)
            return None
        except Exception as e:
            # logging.captureWarnings(True)
            logging.exception(e)

    def __gen_cookies(self, cookie):
        try:
            cs = {}
            for line in cookie.split(';'):
                name, value = line.strip().split('=', 1)
                cs[name] = value
        except:
            pass
        self.cookies = cs


class PythonObjectEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, (list, dict, str, unicode, int, float, bool, type(None))):
            return JSONEncoder.default(self, obj)
        return {'_python_object': pickle.dumps(obj)}


def as_python_object(dct):
    if '_python_object' in dct:
        return pickle.loads(str(dct['_python_object']))
    return dct


class SetObjectEncode(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, set):
            return list(obj)
        if isinstance(obj, (list, dict, str, unicode, int, float, bool, type(None))):
            return json.JSONEncoder.default(self, obj)
        raise TypeError


# class SetObjectDecode(json.JSONDecoder):
#     def default


@singleton
class JsonUtil(object):
    """
    自定义json序列化
    """

    @classmethod
    def encode_json(cls, json_data):
        # print dumps(json_data, cls=PythonObjectEncoder)
        # if hasattr(json_data, '__dict__'):
        return json.dumps(json_data, cls=SetObjectEncode)

    @classmethod
    def decode_json(cls, json_obj):
        return json.loads(json_obj)


if __name__ == '__main__':
    # data = [1, 2, 3, set(['knights', 'who', 'say', 'ni']), {'key': 'value'}, Decimal('3.14')]
    data = set([1, 2, 3, 'a', 'b'])
    json_util = JsonUtil()
    obj = json_util.encode_json(data)
    obj = json_util.decode_json(obj)
    print str(obj) + "--"
